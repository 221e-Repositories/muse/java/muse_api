package _221e.Muse;

public class Range {
    private int lum_vis;
    private int lum_ir;
    private int lux;

    public Range(int arg1, int arg2, int arg3)
    {
        lum_vis = arg1;
        lum_ir = arg2;
        lux = arg3;
    }

    public String toString()
    {
        String str = lux + "\t" + lum_vis + "\t" + lum_ir;
        return str;
    }
}
