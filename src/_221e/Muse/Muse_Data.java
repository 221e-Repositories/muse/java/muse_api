package _221e.Muse;

public class Muse_Data {
    private float[] gyr;
    private float[] axl;
    private float[] mag;
    private float[] hdr;
    private float[] th;
    private float[] tp;
    
    private Range range;
    
    private float sound;
    
    private float[] quat;
    private float[] euler;

    private long timestamp;
    private long overall_timestamp;

    public Muse_Data()
    {
        gyr = new float[3];
        axl = new float[3];
        mag = new float[3];
        hdr = new float[3];
        th = new float[2];
        tp = new float[2];
    
        range = new Range(0,0,0);
        
        sound = 0;

        quat = new float[4];
        euler = new float[3];

        timestamp = 0;
        overall_timestamp = 0;
    }

    public float[] GetGyroscope() {
        return gyr;
    }

    public void SetGyroscope(float x, float y, float z) {
        gyr[0] = x;
        gyr[1] = y;
        gyr[2] = z;
    }

    public void SetGyroscope(float[] value) {
        gyr[0] = value[0];
        gyr[1] = value[1];
        gyr[2] = value[2];
    }

    public float[] GetAccelerometer() {
        return axl;
    }

    public void SetAccelerometer(float x, float y, float z) {
        axl[0] = x;
        axl[1] = y;
        axl[2] = z;
    }

    public void SetAccelerometer(float[] value) {
        axl[0] = value[0];
        axl[1] = value[1];
        axl[2] = value[2];
    }

    public float[] GetMagnetometer() {
        return mag;
    }

    public void SetMagnetometer(float x, float y, float z) {
        mag[0] = x;
        mag[1] = y;
        mag[2] = z;
    }

    public void SetMagnetometer(float[] value) {
        mag[0] = value[0];
        mag[1] = value[1];
        mag[2] = value[2];
    }

    public float[] GetHDR() {
        return gyr;
    }

    public void SetHDR(float x, float y, float z) {
        hdr[0] = x;
        hdr[1] = y;
        hdr[2] = z;
    }

    public void SetHDR(float[] value) {
        hdr[0] = value[0];
        hdr[1] = value[1];
        hdr[2] = value[2];
    }

    public float[] GetTH() {
        return th;
    }

    public void SetTH(float temperature, float humidity) {
        th[0] = temperature;
        th[1] = humidity;
    }

    public void SetTH(float[] value) {
        th[0] = value[0];   // temperature
        th[1] = value[1];   // humidity
    }

    public float[] GetTP() {
        return tp;
    }

    public void SetTP(float temperature, float pressure) {
        tp[0] = temperature;
        tp[1] = pressure;
    }

    public void SetTP(float[] value) {
        tp[0] = value[0];   // temperature;
        tp[1] = value[1];   // pressure;
    }

    public Range GetRange() {
        return range; 
    }

    public void SetRange(Range value) {
        this.range = value;
    }

    public void SetRange(int lum_vis, int lum_ir, int lux) {
        this.range = new Range(lum_vis, lum_ir, lux);
    }

    public void SetRange(int[] value) { // lum_vis, int lum_ir, int lux) {
        this.range = new Range(value[0], value[1], value[2]);
    }

    public float GetSound() {
        return sound; 
    }

    public void SetSound(float value) {
        sound = value;
    }

    public float[] GetQuaternion() {
        return quat;
    }

    public void SetQuaternion(float qw, float qi, float qj, float qk) {
        quat[0] = qw;
        quat[1] = qi;
        quat[2] = qj;
        quat[3] = qk;
    }

    public void SetQuaternion(float[] value) {
        quat[0] = value[0]; // qw
        quat[1] = value[1]; // qi
        quat[2] = value[2]; // qj
        quat[3] = value[3]; // qk
    }

    public float[] GetEulerAngles() {
        return euler;
    }

    public void SetEulerAngles(float roll, float pitch, float yaw) {
        euler[0] = roll;
        euler[1] = pitch;
        euler[2] = yaw;
    }

    public void SetEulerAngles(float[] value) {
        euler[0] = value[0];    // roll
        euler[1] = value[1];    // pitch
        euler[2] = value[2];    // yaw;
    }

    public long GetTimestamp() {
        return timestamp;
    }

    public void SetTimestamp(long value) {
         timestamp = value;
    }

    public long GetOverallTimestamp() {
        return overall_timestamp;
    }

    public void SetOverallTimestamp(long value) {
        overall_timestamp = value;
    }       

    public String ChannelsToString(float[] arg)
    {
        String str = "";

        for (int i = 0; i < arg.length - 1; i++)
            str += arg[i] + "\t";
        str += arg[arg.length - 1];

        return str;
    }

    public String ChannelsToString(int[] arg)
    {
        String str = "";

        for (int i = 0; i < arg.length - 1; i++)
            str += arg[i] + "\t";
        str += arg[arg.length - 1];

        return str;
    }

    public String toString()
    {
        String str =
            overall_timestamp + "\t" +
            timestamp + "\t" +
            ChannelsToString(gyr) + "\t" +
            ChannelsToString(axl) + "\t" +
            ChannelsToString(mag) + "\t" +
            ChannelsToString(hdr) + "\t" +
            ChannelsToString(th) + "\t" +
            ChannelsToString(tp) + "\t" +
            range.toString() + "\t" +
            ChannelsToString(quat);

        return str;
    }
    

}