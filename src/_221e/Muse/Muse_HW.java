package _221e.Muse;

import java.util.HashMap;
import java.util.Map;

public class Muse_HW {

    //////////////////////////////////
    // region CONSTANTS

    /// <summary>Command characteristic size [bytes]</summary>
    public static final int COMM_MESSAGE_LEN_CMD = 20;

    /// <summary>Data characteristic size [bytes]</summary>
    public static final int COMM_MESSAGE_LEN_DAT = 128;

    /// <summary>Timestamp reference epoch</summary>
    /// <para>It corresponds to Sunday 26 January 2020 00:53:20</para>
    public static final long REFERENCE_EPOCH = 1580000000;       // & 0xffffffffL; to interpret long as unsigned

    /// <summary>Bit-mask used to manage read/write command modes</summary>
    public static final byte READ_BIT_MASK = (byte) 0x80;

    // endregion

    //////////////////////////////////
    // region DEVICE

    /// <summary>Command codes</summary>
    public enum Command 
    {
        /// <summary> NOT A COMMAND - used only on software side </summary>
        CMD_NONE((byte)0xff),                       
        /// <summary>Acknowledge</summary>
        CMD_ACK((byte)0x00),                        
        /// <summary>State [get/set]</summary>d
        CMD_STATE((byte)0x02),                      
        /// <summary>Restart</summary>
        CMD_RESTART((byte)0x03),                    
        /// <summary>Application firmware information [readonly]</summary>
        CMD_APP_INFO((byte)0x04),
        /// <summary>Battery Charge [readonly]</summary>
        CMD_BATTERY_CHARGE((byte)0x07),
        /// <summary>Battery Voltage [readonly]</summary>
        CMD_BATTERY_VOLTAGE((byte)0x08),
        /// <summary>Device check up flag [readonly]</summary>
        CMD_CHECK_UP((byte)0x09),
        /// <summary>Installed Firmware Version [readonly]</summary>
        CMD_FW_VERSION((byte)0x0a),
        /// <summary>Current Time [get/set]</summary>
        CMD_TIME((byte)0x0b),
        /// <summary>Bluetooth Module Name [get/set]</summary>
        CMD_BLE_NAME((byte)0x0c),
        /// <summary>Device Identification code [readonly]</summary>
        CMD_DEVICE_ID((byte)0x0e),
        /// <summary>Device features [readonly]</summary>
        CMD_DEVICE_SKILLS((byte)0x0f),
        /// <summary>Memory state [readonly]</summary>
        CMD_MEM_CONTROL((byte)0x20),         
        /// <summary>Get file information [readonly]</summary>
        CMD_MEM_FILE_INFO((byte)0x21),       
        /// <summary>File download</summary>
        CMD_MEM_FILE_DOWNLOAD((byte)0x22),
        /// <summary>Clock offset [get/set]</summary>
        CMD_CLK_OFFSET((byte)0x31),          
        /// <summary>Enter time sync mode</summary>
        CMD_TIME_SYNC((byte)0x32),           
        /// <summary>Exit time sync mode</summary>
        CMD_EXIT_TIME_SYNC((byte)0x33),
        /// <summary>Sensors full scales [get/set]</summary>
        CMD_SENSORS_FS((byte)0x40),
        /// <summary>Sensors calibration matricies [get/set]</summary>
        CMD_CALIB_MATRIX((byte)0x48),
        /// <summary>Log button confiugration [get/set]</summary>
        CMD_BUTTON_LOG((byte)0x50),
        /// <summary> User configuration settings [get/set]</summary>
        CMD_USER_CONFIG((byte)0x51);

        private byte value;
        private static Map map = new HashMap<>();

        private Command(byte value) {
            this.value = value;
        }

        static {
            for (Command item : Command.values()) {
                map.put(item.value, item);
            }
        }

        public static Command valueOf(byte item) {
            return (Command) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Command payload lengths</summary>
    public enum CommandLength {
        /// <summary> Get state command length </summary>
        CMD_LENGTH_GET_STATE(2),
        /// <summary> Set state command length </summary>
        CMD_LENGTH_SET_STATE(3),
        /// <summary> Start stream acquisition command length </summary>
        CMD_LENGTH_START_STREAM(7),
        /// <summary> Start log acquisition command length </summary>
        CMD_LENGTH_START_LOG(7),
        /// <summary> Stop acquisition command length </summary>
        CMD_LENGTH_STOP_ACQUISITION(3),
        /// <summary> Restart command length </summary>
        CMD_LENGTH_RESTART(3),
        /// <summary> Get firmware application info command length </summary>
        CMD_LENGTH_GET_APP_INFO(2),
        /// <summary> Get battery charge level command length </summary>
        CMD_LENGTH_GET_BATTERY_CHARGE(2),
        /// <summary> Get battery voltage level command length </summary>
        CMD_LENGTH_GET_BATTERY_VOLTAGE(2),
        /// <summary> Get check-up register value command length </summary>
        CMD_LENGTH_GET_CHECK_UP(2),
        /// <summary> Get firmware versions command length </summary>
        CMD_LENGTH_GET_FW_VERSION(2),
        /// <summary> Get time command length </summary>
        CMD_LENGTH_GET_TIME(2),
        /// <summary> Set time command length </summary>
        CMD_LENGTH_SET_TIME(6),
        /// <summary> Get device name command length </summary>
        CMD_LENGTH_GET_BLE_NAME(2),
        /// <summary> Set device command length </summary>
        CMD_LENGTH_SET_BLE_NAME(20),
        /// <summary> Get device unique identifier command length </summary>
        CMD_LENGTH_GET_DEVICE_ID(2),
        /// <summary> Get device skills command length </summary>
        CMD_LENGTH_GET_DEVICE_SKILLS(3),
        /// <summary> Get memory status command length </summary>
        CMD_LENGTH_GET_MEM_CONTROL(2),
        /// <summary> Erase memory command length </summary>
        CMD_LENGTH_SET_MEM_CONTROL(3),
        /// <summary> Get memory file info command length </summary>
        CMD_LENGTH_GET_MEM_FILE_INFO(4),
        /// <summary> Start file offload command length </summary>
        CMD_LENGTH_GET_MEM_FILE_DOWNLOAD(5),
        /// <summary> Get clock offset command length </summary>
        CMD_LENGTH_GET_CLK_OFFSET(2),
        /// <summary> Set clock offset command length </summary>
        CMD_LENGTH_SET_CLK_OFFSET(10),
        /// <summary> Enter timesync command length </summary>
        CMD_LENGTH_ENTER_TIME_SYNC(2),
        /// <summary> Exit timesync command length </summary>
        CMD_LENGTH_EXIT_TIME_SYNC(2),
        /// <summary> Get sensors full scale configuration command length </summary>
        CMD_LENGTH_GET_SENSORS_FS(2),
        /// <summary> Set sensors full scale configuration command length </summary>
        CMD_LENGTH_SET_SENSORS_FS(3),
        /// <summary> Get calibration matrix command length </summary>
        CMD_LENGTH_GET_CALIB_MATRIX(4),
        /// <summary> Set calibration matrix command length </summary>
        CMD_LENGTH_SET_CALIB_MATRIX(16),
        /// <summary> Get button log configuration command length </summary>
        CMD_LENGTH_GET_BUTTON_LOG(2),
        /// <summary> Set button log configuration command length </summary>
        CMD_LENGTH_SET_BUTTON_LOG(6),
        /// <summary> Get user configuration command length </summary>
        CMD_LENGTH_GET_USER_CONFIG(2),
        /// <summary> Set user configuration command length </summary>
        CMD_LENGTH_SET_USER_CONFIG(6);

        private int value;
        private static Map map = new HashMap<>();

        private CommandLength(int value) {
            this.value = value;
        }

        static {
            for (CommandLength item : CommandLength.values()) {
                map.put(item.value, item);
            }
        }

        public static CommandLength valueOf(int item) {
            return (CommandLength) map.get(item);
        }
    
        public int getValue() {
            return this.value;
        }
    }

    /// <summary>Acknowledge types</summary>
    public enum AcknowledgeType {
        /// <summary>NOT AN ACKNOWLEDGE - used only on software side </summary>
        ACK_NONE((byte)0xff),
        /// <summary>Success</summary>
        ACK_SUCCESS((byte)0x00),                             
        /// <summary>Error</summary>
        ACK_ERROR((byte)0x01);	
        
        private byte value;
        private static Map map = new HashMap<>();

        private AcknowledgeType(byte value) {
            this.value = value;
        }

        static {
            for (AcknowledgeType item : AcknowledgeType.values()) {
                map.put(item.value, item);
            }
        }

        public static AcknowledgeType valueOf(byte item) {
            return (AcknowledgeType) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }
    
    /// <summary>System states</summary>
    public enum SystemState {
        /// <summary>System state NONE - used only on software side</summary>
        SYS_NONE((byte)0x00),        
        /// <summary>System state ERROR</summary>
        SYS_ERROR((byte)0xff),      
        /// <summary>System state STARTUP</summary>
        SYS_STARTUP((byte)0x01),     
        /// <summary>System state IDLE</summary>
        SYS_IDLE((byte)0x02),        
        /// <summary>System state STANDBY</summary>
        SYS_STANDBY((byte)0x03),     
        /// <summary>System state LOG - acquisition mode</summary>
        SYS_LOG((byte)0x04),         
        /// <summary>System state READOUT - memory file download</summary>
        SYS_READOUT((byte)0x05),     
        /// <summary>System state STREAM - buffered acquisition (realtime)</summary>
        SYS_TX_BUFFERED((byte)0x06), 
        /// <summary>System state CALIB - calibration routines</summary>
        SYS_CALIB((byte)0x07),       
        /// <summary>System state STREAM - direct acquisition (realtime)</summary>
        SYS_TX_DIRECT((byte)0x08);  

        private byte value;
        private static Map map = new HashMap<>();

        private SystemState(byte value) {
            this.value = value;
        }

        static {
            for (SystemState item : SystemState.values()) {
                map.put(item.value, item);
            }
        }

        public static SystemState valueOf(byte item) {
            return (SystemState) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Restart modes</summary>
    public enum RestartMode {
        /// <summary>Restart device in application mode</summary>
        RESET((byte)0x00),
        /// <summary>Restart device in bootloader mode</summary>
        BOOT((byte)0x01);

        private byte value;
        private static Map map = new HashMap<>();

        private RestartMode(byte value) {
            this.value = value;
        }

        static {
            for (RestartMode item : RestartMode.values()) {
                map.put(item.value, item);
            }
        }

        public static RestartMode valueOf(byte item) {
            return (RestartMode) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Communication Channels</summary>
    public enum CommunicationChannel {
        /// <summary>Channel NONE - used only on software side</summary>
        CHANNEL_NONE((byte)0x00),
        /// <summary>Bluetoot Low Energy</summary>
        CHANNEL_BLE((byte)0x01),
        /// <summary>USB</summary>
        CHANNEL_USB((byte)0x02);

        private byte value;
        private static Map map = new HashMap<>();

        private CommunicationChannel(byte value) {
            this.value = value;
        }

        static {
            for (CommunicationChannel item : CommunicationChannel.values()) {
                map.put(item.value, item);
            }
        }

        public static CommunicationChannel valueOf(byte item) {
            return (CommunicationChannel) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    // endregion

    //////////////////////////////////
    // region DATA ACQUISITION

    /// <summary>Acquisition modes</summary>
    public enum DataMode {
        /// <summary>Acquisition mode NONE - used only on software side</summary>
        DATA_MODE_NONE(0x00000000),                 
        /// <summary>Acquisition mode Gyroscope</summary>
        DATA_MODE_GYRO(0x00000001),
        /// <summary>Acquisition mode Accelerometer</summary>
        DATA_MODE_AXL(0x00000002),
        /// <summary>Acquisition mode IMU: Gyroscope + Accelerometer</summary>
        DATA_MODE_IMU(0x00000003),
        /// <summary>Acquisition mode Magnetometer</summary>
        DATA_MODE_MAGN(0x00000004),
        /// <summary>Acquisition mode 9DOF: Gyroscope + Accelerometer + Magnetometer</summary>
        DATA_MODE_9DOF(0x00000007),
        /// <summary>Acquisition mode High Dynamic Range (HDR) Accelerometer</summary>
        DATA_MODE_HDR(0x00000008),
        /// <summary>Acquisition mode IMU + HDR</summary>
        DATA_MODE_IMU_HDR(0x00000011),
        /// <summary>Acquisition mode orientation quaternion</summary>
        DATA_MODE_ORIENTATION(0x00000010),
        /// <summary>Acquisition mode IMU + orientation quaternion</summary>
        DATA_MODE_IMU_ORIENTATION(0x00000013),
        /// <summary>Acquisition mode 9DOF + orientation quaternion</summary>
        DATA_MODE_9DOF_ORIENTATION(0x00000017),
        /// <summary>Acquisition mode timestamp</summary>
        DATA_MODE_TIMESTAMP(0x00000020),
        /// <summary>Acquisition mode temperature and humidity</summary>
        DATA_MODE_TEMP_HUM(0x00000040),
        /// <summary>Acquisition mode temperature and barometric pressure</summary>
        DATA_MODE_TEMP_PRESS(0x00000080),
        /// <summary>Acquisition mode range and light intensity</summary>
        DATA_MODE_RANGE(0x00000100),
        /// <summary>Acquisition mode microphone</summary>
        DATA_MODE_SOUND(0x00000400);

        private int value;
        private static Map map = new HashMap<>();

        private DataMode(int value) {
            this.value = value;
        }

        static {
            for (DataMode item : DataMode.values()) {
                map.put(item.value, item);
            }
        }

        public static DataMode valueOf(int item) {
            return (DataMode) map.get(item);
        }
    
        public int getValue() {
            return this.value;
        }
    }

    /// <summary>Acquisition packet sizes</summary>
    public enum DataSize {
        /// <summary>Packet size Gyroscope</summary>
        DATA_SIZE_GYRO(6),
        /// <summary>Packet size Accelerometer</summary>
        DATA_SIZE_AXL(6),
        /// <summary>Packet size IMU: Gyroscope + Accelerometer</summary>
        DATA_SIZE_IMU(12),
        /// <summary>Packet size Magnetometer</summary>
        DATA_SIZE_MAGN(6),
        /// <summary>Packet size 9DOF: Gyroscope + Accelerometer + Magnetometer</summary>
        DATA_SIZE_9DOF(18),
        /// <summary>Packet size High Dynamic Range (HDR) Accelerometer</summary>
        DATA_SIZE_HDR(6),
        /// <summary>Packet size IMU + HDR</summary>
        DATA_SIZE_IMU_HDR(18),
        /// <summary>Packet size orientation quaternion</summary>
        DATA_SIZE_ORIENTATION(6),
        /// <summary>Packet size IMU + orientation quaternion</summary>
        DATA_SIZE_IMU_ORIENTATION(18),
        /// <summary>Packet size 9DOF + orientation quaternion</summary>
        DATA_SIZE_9DOF_ORIENTATION(24),
        /// <summary>Packet size timestamp</summary>
        DATA_SIZE_TIMESTAMP(6),
        /// <summary>Packet size temperature and humidity</summary>
        DATA_SIZE_TEMP_HUM(6),
        /// <summary>Packet size temperature and barometric pressure</summary>
        DATA_SIZE_TEMP_PRESS(6),
        /// <summary>Packet size luminosity</summary>
        DATA_SIZE_RANGE(6),
        /// <summary>Packet size microphone</summary>
        DATA_SIZE_SOUND(6);   
        
        private int value;
        private static Map map = new HashMap<>();

        private DataSize(int value) {
            this.value = value;
        }

        static {
            for (DataSize item : DataSize.values()) {
                map.put(item.value, item);
            }
        }

        public static DataSize valueOf(int item) {
            return (DataSize) map.get(item);
        }
    
        public int getValue() {
            return this.value;
        }
    }

    /// <summary>Acquisition frequencies</summary>
    public enum DataFrequency {
        /// <summary>Acquisition Frequency NONE - used only on software side</summary>
        DATA_FREQ_NONE((byte)0x00),
        /// <summary>Acquisition frequency 25 Hz</summary>
        DATA_FREQ_25Hz((byte)0x01),
        /// <summary>Acquisition frequency 50 Hz</summary>
        DATA_FREQ_50Hz((byte)0x02),
        /// <summary>Acquisition frequency 100 Hz</summary>
        DATA_FREQ_100Hz((byte)0x04),
        /// <summary>Acquisition frequency 200 Hz</summary>
        DATA_FREQ_200Hz((byte)0x08),
        /// <summary>Acquisition frequency 400 Hz</summary>
        DATA_FREQ_400Hz((byte)0x10),
        /// <summary>Acquisition frequency 800 Hz</summary>
        DATA_FREQ_800Hz((byte)0x20),
        /// <summary>Acquisition frequency 1600 Hz</summary>
        DATA_FREQ_1600Hz((byte)0x40);     
        
        private byte value;
        private static Map map = new HashMap<>();

        private DataFrequency(byte value) {
            this.value = value;
        }

        static {
            for (DataFrequency item : DataFrequency.values()) {
                map.put(item.value, item);
            }
        }

        public static DataFrequency valueOf(byte item) {
            return (DataFrequency) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Acquisition types</summary>
    public enum AcquisitionType {
        /// <summary>Type NONE - used only on software side</summary>
        ACQ_NONE((byte)0x00),
        /// <summary>Buffered - streaming</summary>
        ACQ_TX_BUFFERED((byte)0x01),
        /// <summary>Direct - streaming</summary>
        ACQ_TX_DIRECT((byte)0x02),
        /// <summary>Memory file offload</summary>
        ACQ_READOUT((byte)0x03);

        private byte value;
        private static Map map = new HashMap<>();

        private AcquisitionType(byte value) {
            this.value = value;
        }

        static {
            for (AcquisitionType item : AcquisitionType.values()) {
                map.put(item.value, item);
            }
        }

        public static AcquisitionType valueOf(byte item) {
            return (AcquisitionType) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    // endregion

    //////////////////////////////////
    // region CONFIGURATION

    /// <summary>Button log configuration structure</summary>
    public class ButtonLogConfiguration
    {
        /// <summary>Button log acquisition mode</summary>
        public DataMode md;
        /// <summary>Button log acquisition frequency</summary>
        public DataFrequency freq;

        /// <summary>Button log configuration object constructor</summary>
        public ButtonLogConfiguration(DataMode mode, DataFrequency frequency)
        {
            md = mode;
            freq = frequency;
        }
    }

    /// <summary>Hardware features</summary>
    public enum HardwareSkills {
        /// <summary>Hardware feature NONE - used only on software side</summary>
        SKILLS_HW_NONE(0x0000),
        /// <summary>Gyroscope</summary>
        SKILLS_HW_GYRO(0x0001),
        /// <summary>Accelerometer</summary>
        SKILLS_HW_AXL(0x0002),
        /// <summary>Magnetometer</summary>
        SKILLS_HW_MAGN(0x0004),
        /// <summary>High Dynamic Range (HDR) Accelerometer</summary>
        SKILLS_HW_HDR(0x0008),
        /// <summary>Temperature</summary>
        SKILLS_HW_TEMP(0x0010),
        /// <summary>Relative Humidity</summary>
        SKILLS_HW_RH(0x0020),
        /// <summary>Barometric Pressure</summary>
        SKILLS_HW_BAR(0x0040),
        /// <summary>Light intensity (i.e., visible)</summary>
        SKILLS_HW_LUM_VIS(0x0080),
        /// <summary>Light intensity (i.e., infrared)</summary>
        SKILLS_HW_LUM_IR(0x0100),
        /// <summary>Distance / Range</summary>
        SKILLS_HW_RANGE(0x0200),
        /// <summary>Microphone</summary>
        SKILLS_HW_MIC(0x0400);

        private int value;
        private static Map map = new HashMap<>();

        private HardwareSkills(int value) {
            this.value = value;
        }

        static {
            for (HardwareSkills item : HardwareSkills.values()) {
                map.put(item.value, item);
            }
        }

        public static HardwareSkills valueOf(int item) {
            return (HardwareSkills) map.get(item);
        }
    
        public int getValue() {
            return this.value;
        }
    }

    /// <summary>Sensors identifiers within the system</summary>
    public enum MEMS_ID {
        /// <summary>Gyroscope</summary>
        SENSORS_GYRO((byte)0x01),
        /// <summary>Accelerometer</summary>
        SENSORS_AXL((byte)0x02),
        /// <summary>High Dynamic Range (HDR) Accelerometer</summary>
        SENSORS_HDR((byte)0x04),
        /// <summary>Magnetometer</summary>
        SENSORS_MAGN((byte)0x08);
        
        private byte value;
        private static Map map = new HashMap<>();

        private MEMS_ID(byte value) {
            this.value = value;
        }

        static {
            for (MEMS_ID item : MEMS_ID.values()) {
                map.put(item.value, item);
            }
        }

        public static MEMS_ID valueOf(byte item) {
            return (MEMS_ID) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Sensors full scale bit-mask</summary>
    public enum MEMS_FullScaleMask {
        /// <summary>Gyroscope</summary>
        SENSORSFS_MASK_GYRO(0x03),
        /// <summary>Accelerometer</summary>
        SENSORSFS_MASK_AXL(0x0c),
        /// <summary>High Dynamic Range (HDR) Accelerometer</summary>
        SENSORSFS_MASK_HDR(0x30),
        /// <summary>Magnetometer</summary>
        SENSORSFS_MASK_MAGN(0xc0);

        private int value;
        private static Map map = new HashMap<>();

        private MEMS_FullScaleMask(int value) {
            this.value = value;
        }

        static {
            for (MEMS_FullScaleMask item : MEMS_FullScaleMask.values()) {
                map.put(item.value, item);
            }
        }

        public static MEMS_FullScaleMask valueOf(int item) {
            return (MEMS_FullScaleMask) map.get(item);
        }
    
        public int getValue() {
            return this.value;
        }
    }

    /// <summary>Gyroscope full scales</summary>
    public enum GyroscopeFS {
        /// <summary>245 dps</summary>
        GYR_FS_245dps((byte)0x00),
        /// <summary>500 dps</summary>
        GYR_FS_500dps((byte)0x01),
        /// <summary>1000 dps</summary>
        GYR_FS_1000dps((byte)0x02),
        /// <summary>2000 dps</summary>
        GYR_FS_2000dps((byte)0x03);

        private byte value;
        private static Map map = new HashMap<>();

        private GyroscopeFS(byte value) {
            this.value = value;
        }

        static {
            for (GyroscopeFS item : GyroscopeFS.values()) {
                map.put(item.value, item);
            }
        }

        public static GyroscopeFS valueOf(byte item) {
            return (GyroscopeFS) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Gyroscope configurations dictionary (i.e., full scale and sensitivity coefficient)</summary>
    public static Map<Byte,SensorConfig> Gyroscope_CFG = new HashMap<Byte,SensorConfig>() {{
        put((byte)0x00, new SensorConfig(245, 0.00875f));
        put((byte)0x01, new SensorConfig(500, 0.0175f));
        put((byte)0x02, new SensorConfig(1000, 0.035f));
        put((byte)0x03, new SensorConfig(2000, 0.070f));
    }};

    /// <summary>Accelerometer full scales</summary>
    public enum AccelerometerFS {
        /// <summary>4 g</summary>
        AXL_FS_4g((byte)0x00),
        /// <summary>8 g</summary>
        AXL_FS_08g((byte)0x08),
        /// <summary>16 g</summary>
        AXL_FS_16g((byte)0x0c),
        /// <summary>32 g</summary>
        AXL_FS_32g((byte)0x04);

        private byte value;
        private static Map map = new HashMap<>();

        private AccelerometerFS(byte value) {
            this.value = value;
        }

        static {
            for (AccelerometerFS item : AccelerometerFS.values()) {
                map.put(item.value, item);
            }
        }

        public static AccelerometerFS valueOf(byte item) {
            return (AccelerometerFS) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Accelerometer configurations dictionary (i.e., full scale and sensitivity coefficient)</summary>
    public static Map<Byte,SensorConfig> Accelerometer_CFG = new HashMap<Byte,SensorConfig>() {{
        put((byte)0x00, new SensorConfig(4, 0.122f));
        put((byte)0x08, new SensorConfig(8, 0.244f));
        put((byte)0x0c, new SensorConfig(16, 0.488f));
        put((byte)0x04, new SensorConfig(32, 0.976f));
    }};

    /// <summary>Magnetometer full scales</summary>
    public enum MagnetometerFS {
        /// <summary>4 Gauss</summary>
        MAG_FS_04G((byte)0x00),
        /// <summary>8 Gauss</summary>
        MAG_FS_08G((byte)0x40),
        /// <summary>12 Gauss</summary>
        MAG_FS_12G((byte)0x80),
        /// <summary>16 Gauss</summary>
        MAG_FS_16G((byte)0xc0);

        private byte value;
        private static Map map = new HashMap<>();

        private MagnetometerFS(byte value) {
            this.value = value;
        }

        static {
            for (MagnetometerFS item : MagnetometerFS.values()) {
                map.put(item.value, item);
            }
        }

        public static MagnetometerFS valueOf(byte item) {
            return (MagnetometerFS) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>Magnetometer configurations dictionary (i.e., full scale and sensitivity coefficient)</summary>
    public static Map<Byte,SensorConfig> Magnetometer_CFG = new HashMap<Byte,SensorConfig>() {{
        put((byte)0x00, new SensorConfig(4, 1000.0f/6842.0f));
        put((byte)0x40, new SensorConfig(8, 1000.0f/3421.0f));
        put((byte)0x80, new SensorConfig(12, 1000.0f/2281.0f));
        put((byte)0xc0, new SensorConfig(16, 1000.0f/1711.0f));
    }};

    /// <summary>High Dynamic Range (HDR) Accelerometer full scales</summary>
    public enum AccelerometerHDRFS {
        /// <summary>100 g</summary>
        HDR_FS_100g((byte)0x00),
        /// <summary>200 g</summary>
        HDR_FS_200g((byte)0x10),
        /// <summary>400 g</summary>
        HDR_FS_400g((byte)0x30);

        private byte value;
        private static Map map = new HashMap<>();

        private AccelerometerHDRFS(byte value) {
            this.value = value;
        }

        static {
            for (AccelerometerHDRFS item : AccelerometerHDRFS.values()) {
                map.put(item.value, item);
            }
        }

        public static AccelerometerHDRFS valueOf(byte item) {
            return (AccelerometerHDRFS) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    /// <summary>High Dynamic Range (HDR) Accelerometer configurations dictionary (i.e., full scale and sensitivity coefficient)</summary>
    public static Map<Byte,SensorConfig> AccelerometerHDR_CFG = new HashMap<Byte,SensorConfig>() {{
        put((byte)0x00, new SensorConfig(100, 49.0f));
        put((byte)0x10, new SensorConfig(200, 98.0f));
        put((byte)0x30, new SensorConfig(400, 195.0f));
    }};

    /// <summary>Bit-masks for user configuration encoding / decoding operations</summary>
    public enum UserConfigMask {
        /// <summary>NONE - used only on software side</summary>
        USER_CFG_MASK_NONE((short)0x0000),
        /// <summary>Extracts STANDBY configuration channel</summary>
        USER_CFG_MASK_STANDBY((short)0x0001),
        /// <summary>Extracts CIRCULAR MEMORY configuration channel</summary>
        USER_CFG_MASK_CIRCULAR_MEMORY((short)0x0002),
        /// <summary>Extracts USB STREAM configuration channel</summary>
        USER_CFG_MASK_USB_STREAM((short)0x0004);

        private short value;
        private static Map map = new HashMap<>();

        private UserConfigMask(short value) {
            this.value = value;
        }

        static {
            for (UserConfigMask item : UserConfigMask.values()) {
                map.put(item.value, item);
            }
        }

        public static UserConfigMask valueOf(short item) {
            return (UserConfigMask) map.get(item);
        }
    
        public short getValue() {
            return this.value;
        }
    }

    // endregion

    //////////////////////////////////
    // region CALIBRATION

    /// <summary>Calibration types</summary>
    public enum CalibrationType {
        /// <summary>Accelerometer</summary>
        CALIB_TYPE_AXL((byte)0),
        /// <summary>Gyroscope</summary>
        CALIB_TYPE_GYR((byte)1),
        /// <summary>Magnetometer</summary>
        CALIB_TYPE_MAG((byte)2);

        private byte value;
        private static Map map = new HashMap<>();

        private CalibrationType(byte value) {
            this.value = value;
        }

        static {
            for (CalibrationType item : CalibrationType.values()) {
                map.put(item.value, item);
            }
        }

        public static CalibrationType valueOf(byte item) {
            return (CalibrationType) map.get(item);
        }
    
        public byte getValue() {
            return this.value;
        }
    }

    // endregion
}
