package _221e.Muse;

public class UserConfig {
    private boolean StandbyEnabled; // { get; private set; }
    private boolean CircularMemoryEnabled; // { get; private set; }
    private boolean USBStreamEnabled; // { get; private set; }

    /// <summary>Standby enable/disable flag</summary>
    public boolean GetStandbyEnabled() {
        return StandbyEnabled;
    } 

    /// <summary>Circular Memory enable/disable flag</summary>
    public boolean GetCircularMemoryEnabled() {
        return CircularMemoryEnabled;
    }

    /// <summary>USB stream enable/disable flag</summary>
    public boolean GetUSBStreamEnabled() {
        return USBStreamEnabled;
    }
    
    /// <summary>UserConfig object constructor</summary>
    /// <param name="standby">Boolean enabled/disabled standby status</param>
    /// <param name="memory">Boolean enabled/disabled circular memory status</param>
    /// <param name="usb">Boolean enabled/disabled USB stream status</param>
    public UserConfig(boolean standby, boolean memory, boolean usb)
    {
        StandbyEnabled = standby;
        CircularMemoryEnabled = memory;
        USBStreamEnabled = usb;
    }

    /// <summary>Override of ToString method</summary>
    public String toString()
    {
        return "";
    }
}
