package _221e.Muse;

/// <summary>Command Response structure</summary>
    public class CommandResponse
    {
        private short rx; // { get; private set; }
        private short tx; // { get; private set; }
        private short len; // { get; private set; }
        private short ack; // { get; private set; }
        private byte[] payload; // { get; private set; }

        /// <summary>Acknowledge response command</summary>
        public short Rx() // { get; private set; }
        {
            return rx;
        } 

        /// <summary>Command on which the acknowledge has been sent</summary>
        public short Tx() // { get; private set; }
        {
            return tx;
        }

        /// <summary>Payload length</summary>
        public short Len() // { get; private set; }
        {
            return len;
        }

        /// <summary>Acknowledge error code</summary>
        public short Ack() // { get; private set; }
        {
            return ack;
        }

        /// <summary>Payload content</summary>
        public byte[] Payload() // { get; private set; }
        {
            return payload;            
        }

        /// <summary>CommandResponse constructor</summary>
        /// <param name="buffer">Byte array representation of the received command response</param>
        public CommandResponse(byte[] buffer)
        {
            if (buffer != null)
            {
                // Convert raw byte array buffer into the corresponding unsigned integer representation    
                short[] uBuffer = new short[buffer.length];

                for(int i=0; i<buffer.length; i++) {
                    uBuffer[i] = (short)(((short)buffer[i]) & 0xff);
                }

                rx = uBuffer[0];
                tx = uBuffer[2];

                len = uBuffer[1];

                ack = uBuffer[3];

                payload = new byte[len - 2];
                // Copy raw buffer, without any signed / unsigned data type conversion
                System.arraycopy(buffer, 4, payload, 0, len-2); 
            }
            else
            {
                rx = -1;
                tx = -1;
                len = -1;
                ack = -1;
                payload = null;
            }
        }
    }
