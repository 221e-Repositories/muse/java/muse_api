package _221e.Muse;

/// <summary>Application info structure</summary>
public class ApplicationInfo
{
    private long crc;
    private long length;

    /// <summary>Ciclic Redundancy Check (CRC)</summary>
    public long CRC() {
        return crc;
    }

    /// <summary>Binary file length (bytes)</summary>
    public long Length() {
        return length;
    } 

    /// <summary>ApplicationInfo constructor</summary>
    /// <param name="crc">CRC value</param>
    /// <param name="length">Length of file (num of bytes)</param>
    public ApplicationInfo(long crc, long length)
    {
        this.crc = crc;
        this.length = length;
    }

    /// <summary>Override of ToString method</summary>
    public String toString()
    {
        return crc + ", " +  length;
    }
}

