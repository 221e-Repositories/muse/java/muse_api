package _221e.Muse;

/// <summary>Sensors configuration structure</summary>
public class SensorConfig
{
    /// <summary>Full scale integer value</summary>
    private int fullScale; // { get; private set; }
    /// <summary>Sensitivity coefficient floating point value</summary>
    private float sensitivity; // { get; private set; }

    /// <summary>MEMS configuration object constructor</summary>
    /// <param name="fs">Full scale</param>
    /// <param name="sens">Sensitivity</param>
    public SensorConfig(int fs, float sens)
    {
        fullScale = fs;
        sensitivity = sens;
    }

    public int FullScale() {
        return fullScale;
    }

    public float Sensitivity() {
        return sensitivity;
    }

    /// <summary>Override of ToString method</summary>
    public String ToString()
    {
        return fullScale + ", " + sensitivity;
    }
}