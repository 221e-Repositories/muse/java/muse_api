package _221e.Muse;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import _221e.Muse.Muse_HW.AccelerometerFS;
import _221e.Muse.Muse_HW.AccelerometerHDRFS;
import _221e.Muse.Muse_HW.AcknowledgeType;
import _221e.Muse.Muse_HW.AcquisitionType;
import _221e.Muse.Muse_HW.Command;
import _221e.Muse.Muse_HW.CommandLength;
import _221e.Muse.Muse_HW.CommunicationChannel;
import _221e.Muse.Muse_HW.DataFrequency;
import _221e.Muse.Muse_HW.DataMode;
import _221e.Muse.Muse_HW.DataSize;
import _221e.Muse.Muse_HW.GyroscopeFS;
import _221e.Muse.Muse_HW.MEMS_FullScaleMask;
import _221e.Muse.Muse_HW.MagnetometerFS;
import _221e.Muse.Muse_HW.RestartMode;
import _221e.Muse.Muse_HW.SystemState;
import _221e.Muse.Muse_HW.UserConfigMask;

public class Muse_Utils {
    //////////////////////////////////
    // region USB WRAPPER FUNCTIONS

    /// <summary>Adds header and trailer to a standard command.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="buffer">Input byte array to be wrapped.</param>
    private static byte[] WrapMessage(byte[] buffer)
    {
        byte[] wrapped_buffer = null;
        if (buffer != null)
        {
            // Define wrapped message buffer
            wrapped_buffer = new byte[buffer.length + 4];

            // Add header to input buffer
            wrapped_buffer[0] = (byte)'?';
            wrapped_buffer[1] = (byte)'!';

            // Copy main content
            System.arraycopy(buffer, 0, wrapped_buffer, 2, buffer.length);

            // Add trailer to input buffer
            wrapped_buffer[wrapped_buffer.length - 2] = (byte)'!';
            wrapped_buffer[wrapped_buffer.length - 1] = (byte)'?';
        }
        return wrapped_buffer;
    }

    /// <summary>Removes header and trailer from a standard command.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="buffer">Input byte array from which the header and trailer must be removed.</param>
    private static byte[] ExtractMessage(byte[] buffer)
    {
        // Remove header and trailer from input buffer
        byte[] dewrapped_buffer = null;
        if (buffer != null)
            System.arraycopy(buffer, 2, dewrapped_buffer, 0, buffer.length - 4);
        return dewrapped_buffer;
    }

    // endregion

    //////////////////////////////////
    // region ENCODING COMMAND IMPLEMENTATION

    // CMD_ACK = 0x00
    /*
    public static byte[] Cmd_Acknowledge(AcknowledgeType ack)
    {
        // Definition of message buffer
        byte[] buffer = new byte[COMM_MESSAGE_LEN_CMD];
        return buffer;
    }
    */

    /// <summary>Builds command to retrieve current system state.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetSystemState(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_STATE.getValue()];
       
        // Get state command
        buffer[0] = (byte)(Command.CMD_STATE.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }
    
    /// <summary>Builds command to start acquisition in stream mode.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="mode">Identifies the set of data to be acquired.</param>
    /// <param name="frequency">Identifies the data acquisition frequency.</param>
    /// <param name="enableDirect">Allows to select the stream type (i.e., direct or buffered). </param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_StartStream(int mode, DataFrequency frequency, boolean enableDirect, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_START_STREAM.getValue()];

        // Start stream acquisition using set state command
        buffer[0] = Command.CMD_STATE.getValue();

        // Set payload length
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_START_STREAM.getValue() - 2);

        // Set tx type based on boolean flag value
        if (enableDirect)
            buffer[2] = SystemState.SYS_TX_DIRECT.getValue();
        else
            buffer[2] = SystemState.SYS_TX_BUFFERED.getValue();

        // Set acquisition mode
        byte[] tmp = new byte[4];
        tmp = intToBytes(mode);
        System.arraycopy(tmp, 0, buffer, 3, 3);

        // Set acquisition frequency
        buffer[6] = frequency.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to start acquisition in log mode.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="mode">Identifies the set of data to be acquired.</param>
    /// <param name="frequency">Identifies the data acquisition frequency.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_StartLog(int mode, DataFrequency frequency, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_START_LOG.getValue()];

        // Start stream acquisition using set state command
        buffer[0] = Command.CMD_STATE.getValue();

        // Set payload length
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_START_STREAM.getValue() - 2);

        // Set state - LOG
        buffer[2] = SystemState.SYS_LOG.getValue();

        // Set acquisition mode
        byte[] tmp = new byte[4];
        tmp = intToBytes(mode);
        System.arraycopy(tmp, 0, buffer, 3, 3);

        // Set acquisition frequency
        buffer[6] = frequency.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to stop any data acquisition procedure.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_StopAcquisition(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_STOP_ACQUISITION.getValue()];

        // Set IDLE state to stop any acquisition procedure
        buffer[0] = Command.CMD_STATE.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_STOP_ACQUISITION.getValue() - 2);
        buffer[2] = SystemState.SYS_IDLE.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to set restart device.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="restartMode">Allows to select the restart mode (i.e., reset or boot).</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_Restart(RestartMode restartMode, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_RESTART.getValue()];
        
        // Set RESTART command with the specified restart mode (i.e., boot or app)
        buffer[0] = Command.CMD_RESTART.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_RESTART.getValue() - 2);
        buffer[2] = restartMode.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrive application firmware information.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetApplicationInfo(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_APP_INFO.getValue()];

        // Get firmware application info
        buffer[0] = (byte)(Command.CMD_APP_INFO.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve battery charge level [%].</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetBatteryCharge(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_BATTERY_CHARGE.getValue()];

        // Get battery charge
        buffer[0] = (byte)(Command.CMD_BATTERY_CHARGE.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve battery voltage level [mV].</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetBatteryVoltage(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_BATTERY_VOLTAGE.getValue()];

        // Get battery voltage
        buffer[0] = (byte)(Command.CMD_BATTERY_VOLTAGE.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve system check-up register value.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetDeviceCheckup(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_CHECK_UP.getValue()];

        // Get check up register value
        buffer[0] = (byte)(Command.CMD_CHECK_UP.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve current firmware versions (i.e., both bootloader and application).</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetFirmwareVersion(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_FW_VERSION.getValue()];

        // Get firmware version labels (i.e., bootloader and application firmware)
        buffer[0] = (byte)(Command.CMD_FW_VERSION.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to update date/time.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_SetTime(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_TIME.getValue()];

        // Set time using current timespan since 1970
        buffer[0] = Command.CMD_TIME.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_TIME.getValue() - 2);

        // Get current timespan since 1/1/1970
        long secondsSinceEpoch = System.currentTimeMillis() / 1000;

        // Set payload - seconds since epoch (4 bytes)
        byte[] payload = longToBytes(secondsSinceEpoch);
        System.arraycopy(payload, 0, buffer, 2, CommandLength.CMD_LENGTH_SET_TIME.getValue() - 2);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve current date/time.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetTime(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_TIME.getValue()];

        // Get current datetime
        buffer[0] = (byte)(Command.CMD_TIME.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to change the device name (i.e., the name advertised by the Bluetooth module).</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_SetDeviceName(String bleName, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_BLE_NAME.getValue()];

        // Check bleName to be set consistency before proceeding
        if (bleName != "" && bleName.length() < 16)
        {
            // int respLen = bleName.length();
            // buffer = new byte[respLen + 2];

            buffer[0] = Command.CMD_BLE_NAME.getValue();
            
            int respLen = bleName.length();
            buffer[1] = (byte)respLen;

            byte[] tmpName = bleName.getBytes(StandardCharsets.US_ASCII);
            System.arraycopy(tmpName,0,buffer,2,respLen);
        }

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve device name (i.e., the name advertised by the Bluetooth module).</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetDeviceName(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_BLE_NAME.getValue()];

        // Get device name (i.e., ble name)
        buffer[0] = (byte)(Command.CMD_BLE_NAME.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the device unique identifier.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetDeviceID(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_DEVICE_ID.getValue()];

        // Get device unique identifier
        buffer[0] = (byte)(Command.CMD_DEVICE_ID.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the devices skills (i.e. hardware or software features provided by the device). By default hardware skills are provided.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="softwareSkills">Allows to select the type of features of interest enabling or disabling software skills flag.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetDeviceSkills(boolean softwareSkills, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_DEVICE_SKILLS.getValue()];

        // Get device skills (i.e., hardware or software based on specified flag)
        buffer[0] = (byte)(Command.CMD_DEVICE_SKILLS.getValue() + Muse_HW.READ_BIT_MASK);
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_GET_DEVICE_SKILLS.getValue() - 2);

        if (softwareSkills)
            buffer[2] = 0x01;

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve current memory status.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetMemoryStatus(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_MEM_CONTROL.getValue()];

        // Get available memory
        buffer[0] = (byte)(Command.CMD_MEM_CONTROL.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to erase device memory.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="bulk_erase"></param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_EraseMemory(int bulk_erase, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_MEM_CONTROL.getValue()];

        // Erase memory 
        buffer[0] = Command.CMD_MEM_CONTROL.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_MEM_CONTROL.getValue() - 2);
        buffer[2] = (byte)bulk_erase;

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve file information.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="fileId">File identifier.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_MemoryFileInfo(int fileId, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_MEM_FILE_INFO.getValue()];

        // Retrieve file information given a specific file identifier
        buffer[0] = (byte)(Command.CMD_MEM_FILE_INFO.getValue() + Muse_HW.READ_BIT_MASK);
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_GET_MEM_FILE_INFO.getValue() - 2);

        // Set file id as a 2-bytes unsigned integer value
        byte[] valueBytes = shortToBytes((short)fileId);
        System.arraycopy(valueBytes,0,buffer,2,2);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to activate a memory file offload procedure.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="fileId">File identifier.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_MemoryFileDownload(int fileId, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_MEM_FILE_DOWNLOAD.getValue()];

        // Start file offload procedure
        buffer[0] = Command.CMD_MEM_FILE_DOWNLOAD.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_GET_MEM_FILE_DOWNLOAD.getValue() - 2);

        // Set file identifier
        byte[] valueBytes = shortToBytes((short)fileId);
        System.arraycopy(valueBytes,0,buffer,2,2);

        // Set file offload channel (USB vs BLE)
        buffer[4] = channel.getValue();
        if (channel == Muse_HW.CommunicationChannel.CHANNEL_USB)
            buffer[4] = 0x00; // set by default to USB channel (if 0x01 it manages the BLE transfer)

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to trigger a clock offset estimation procedure.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="inOffset">Allows to specify a custom clock offset to be set.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_SetClockOffset(long inOffset, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_CLK_OFFSET.getValue()];

        // Set clock offset
        buffer[0] = Command.CMD_CLK_OFFSET.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_CLK_OFFSET.getValue() - 2);

        byte[] valueBytes = longToBytes(inOffset);
        System.arraycopy(valueBytes,0,buffer,2,8);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve current clock offset.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_GetClockOffset(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_CLK_OFFSET.getValue()];

        // Get clock offset
        buffer[0] = (byte)(Command.CMD_CLK_OFFSET.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to enter timesync routine.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_EnterTimeSync(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_ENTER_TIME_SYNC.getValue()];

        // Start timesync procedure
        buffer[0] = Command.CMD_TIME_SYNC.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to exit timesync routine.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>      
    public static byte[] Cmd_ExitTimeSync(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_EXIT_TIME_SYNC.getValue()];

        // Stop timesync procedure
        buffer[0] = Command.CMD_EXIT_TIME_SYNC.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to set a custom sensors full scale configuration.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="gyrFS">Gyroscope full scale code.</param>
    /// <param name="axlFS">Accelerometer full scale code.</param>
    /// <param name="magFS">Magnetometer full scale code.</param>
    /// <param name="hdrFS">High Dynamic Range (HDR) Accelerometer code.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    public static byte[] Cmd_SetSensorsFullScale(GyroscopeFS gyrFS, AccelerometerFS axlFS, MagnetometerFS magFS, AccelerometerHDRFS hdrFS, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_SENSORS_FS.getValue()];

        // Set sensors full scale configuration
        buffer[0] = Command.CMD_SENSORS_FS.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_SENSORS_FS.getValue() - 2);

        // Build integer configuration code to be sent
        buffer[2] = (byte)(axlFS.getValue() | gyrFS.getValue() | hdrFS.getValue() | magFS.getValue());

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the current sensors full scale configuration.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param>   
    public static byte[] Cmd_GetSensorsFullScale(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_SENSORS_FS.getValue()];

        // Get current sensors full scale configuration
        buffer[0] = (byte)(Command.CMD_SENSORS_FS.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    // CMD_CALIB_MATRIX = 0x48
    public static byte[] Cmd_SetCalibrationMatrix(byte rowId, byte colId, float r1, float r2, float r3, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_CALIB_MATRIX.getValue()];

        // Set calibration matrix components
        buffer[0] = Command.CMD_CALIB_MATRIX.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_CALIB_MATRIX.getValue() - 2);

        // Payload
        buffer[2] = rowId;                                  // Row index in the range 0-2
        buffer[3] = colId;                                  // Column index in the range 0-3

        // Values for a given row and col
        byte[] valByteArray = floatToBytes(r1);
        System.arraycopy(valByteArray, 0, buffer, 4, 4);          // Value 1 - row 0
        valByteArray = floatToBytes(r2);
        System.arraycopy(valByteArray, 0, buffer, 8, 4);          // Value 2 - row 1
        valByteArray = floatToBytes(r3);
        System.arraycopy(valByteArray, 0, buffer, 12, 4);         // Value 3 - row 2

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    public static byte[] Cmd_GetCalibrationMatrix(byte rowId, byte colId, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_CALIB_MATRIX.getValue()];

        // Get current calibration matrix values
        buffer[0] = (byte)(Command.CMD_CALIB_MATRIX.getValue() + Muse_HW.READ_BIT_MASK);
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_GET_CALIB_MATRIX.getValue() - 2);

        // Set row/col payload indexes to be retrieved
        buffer[2] = rowId;
        buffer[3] = colId;

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to configure a default log mode controlled using the smart button.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="mode">Identifies the set of data to be acquired.</param>
    /// <param name="frequency">Identifies the data acquisition frequency.</param>
    /// <param name="channel">Communication channel over which the command will be sent.</param> 
    public static byte[] Cmd_SetButtonLogConfiguration(int mode, DataFrequency frequency, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_BUTTON_LOG.getValue()];
        
        // Set log mode command
        buffer[0] = Command.CMD_BUTTON_LOG.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_BUTTON_LOG.getValue() - 2);

        // Set log mode code
        byte[] tmp = new byte[3];
        tmp = intToBytes(mode);
        System.arraycopy(tmp, 0, buffer, 2, 3);

        // Set log frequency
        buffer[5] = frequency.getValue();

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the current log mode.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param> 
    public static byte[] Cmd_GetButtonLogConfiguration(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_BUTTON_LOG.getValue()];

        // Get current log mode
        buffer[0] = (byte)(Command.CMD_BUTTON_LOG.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the current log mode.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="bitMask">16-bit unsigned integer representing the configuration channels to be set.</param> 
    /// <param name="standby">Boolean flag that allows to enable / disable automatic standby.</param> 
    /// <param name="circular">Boolean flag that allows to enable / disable circular memory management.</param> 
    /// <param name="usbStream">Boolean flag that allows to set default streaming channel to USB.</param> 
    /// <param name="channel">Communication channel over which the command will be sent.</param> 
    public static byte[] Cmd_SetUserConfiguration(int bitMask, boolean standby, boolean circular, boolean usbStream, CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_SET_USER_CONFIG.getValue()];

        // Set a custom user configuration
        buffer[0] = Command.CMD_USER_CONFIG.getValue();
        buffer[1] = (byte)(CommandLength.CMD_LENGTH_SET_USER_CONFIG.getValue() - 2);

        // Set bit-mask from UserConfigMask enum
        byte[] tmp = new byte[2];
        tmp = shortToBytes((short)bitMask);
        System.arraycopy(tmp, 0, buffer, 2, 2);

        // Build configuration code to be sent using user input
        short config_code = 0;
        if (standby)
            config_code |= 0x0001;

        if (circular)
            config_code |= 0x0002;

        if (usbStream)
            config_code |= 0x0004;

        // Set configuration code
        tmp = shortToBytes(config_code);
        System.arraycopy(tmp, 0, buffer, 4, 2);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    /// <summary>Builds command to retrieve the current user configuration parameters.</summary>
    /// <returns>A byte array.</returns>
    /// <param name="channel">Communication channel over which the command will be sent.</param> 
    public static byte[] Cmd_GetUserConfiguration(CommunicationChannel channel)
    {
        // Definition of message buffer
        byte[] buffer = new byte[CommandLength.CMD_LENGTH_GET_USER_CONFIG.getValue()];

        // Get current user configuration
        buffer[0] = (byte)(Command.CMD_USER_CONFIG.getValue() + Muse_HW.READ_BIT_MASK);

        // Wrap message with header and trailer in the case of USB communication
        if (channel == CommunicationChannel.CHANNEL_USB)
            return WrapMessage(buffer);

        return buffer;
    }

    // endregion

    //////////////////////////////////
    // region DECODING FUNCTIONS

    /// <summary>Parse command characteristic to get a command response object.</summary>
    /// <param name="channel">Communication channel over which the command will be sent.</param>
    /// <param name="buffer">Byte array to be parsed.</param>
    /// <return>CommandResponse (output) object.</param>
    public static CommandResponse ParseCommandCharacteristic(CommunicationChannel channel, byte[] buffer)
    {
        // Build a command response container given the command characteristic content as a byte array
        // Manage also header and trailer removal in case of BLE or USB channel
        CommandResponse response = null;
        if (channel == CommunicationChannel.CHANNEL_BLE)
            response = new CommandResponse(buffer);
        else
            response = new CommandResponse(ExtractMessage(buffer));

        return response;
    }

    /// <summary>Decode system state.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>SystemState value.</param>
    public static SystemState Dec_SystemState(CommandResponse response)
    {
        // Decode system state given command response
        if ((response.Tx() & 0x7F) == Command.CMD_STATE.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                return Muse_HW.SystemState.valueOf(response.Payload()[0]);
        }

        return null;
    }

    /// <summary>Decode firmware application information.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>ApplicationInfo object containing CRC and file length values.</param>
    public static ApplicationInfo Dec_ApplicationInfo(CommandResponse response)
    {
        // Decode firmware application info given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_APP_INFO.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                long crc = bytesToUInt32(response.Payload(),0);
                long length = bytesToUInt32(response.Payload(),4);
                return new ApplicationInfo(crc,length);
            }
        }

        return null;
    }

    /// <summary>Decode battery charge level.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Battery charge [%] value.</param>
    public static byte Dec_BatteryCharge(CommandResponse response)
    {
        byte charge = -1;

        // Decode battery charge percentage value given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_BATTERY_CHARGE.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                charge = (byte)(response.Payload()[0] & 0xff);
        }

        return charge;
    }

    /// <summary>Decode battery voltage level.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Battery voltage [mV] value.</param>
    public static short Dec_BatteryVoltage(CommandResponse response)
    {
        short voltage = -1;

        // Decode battery voltage [mV] value given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_BATTERY_VOLTAGE.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                voltage = (short)bytesToUInt16(response.Payload(),0);
        }

        return voltage;
    }

    /// <summary>Decode check-up register code.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Check-up register code.</param>
    public static String Dec_CheckUp(CommandResponse response)
    {
        // Decode checkup register value, as string, given the command response payload
        String code = "";

        // Decode current checkup register value given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_CHECK_UP.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                code = bytesToHex(response.Payload());
        }

        return code;
    }

    /// <summary>Decode firmware version labels.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Boot loader and application firmware version labels.</param>
    public static String[] Dec_FirmwareVersion(CommandResponse response)
    {
        // Decode current firmware versions given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_FW_VERSION.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                String[] fw_rev = new String[3];

                // Decode bootloader and application firmware versions
                fw_rev[0] = new String(response.Payload(),0,6); // boot_rev
                fw_rev[1] = new String(response.Payload(),7,6);   // app_rev
                fw_rev[2] = (response.Payload()[14] & 0xff) + "." + (response.Payload()[15] & 0xff);

                return fw_rev;
            }
        }

        return null;
    }

    /// <summary>Decode date/time.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Date/time object.</param>
    public static Date Dec_DateTime(CommandResponse response)
    {
        // Decode current Date/Time given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_TIME.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                long seconds = bytesToUInt32(response.Payload(),0);
                return new Date(seconds*1000);  // x1000 to properly manage ms resolution
            }
        }

        return null;
    }

    /// <summary>Decode device name (i.e., it is the name advertised by Bluetooth module).</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Device name.</param>
    public static String Dec_DeviceName(CommandResponse response)
    {
        String name = "";

        // Decode current device name (i.e., ble name) given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_BLE_NAME.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                name = new String(response.Payload());
        } 

        return name;
    }

    /// <summary>Decode device unique identifier.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Device unique identifier.</param>
    public static String Dec_DeviceID(CommandResponse response)
    {
        String id = "";

        // Decode current device unique identifier given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_DEVICE_ID.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0) {
                // Revert array before printing string in order to keep endianness in byte representation
                byte[] flipped_payload = reverseBytes(response.Payload());
                id = bytesToHex(flipped_payload);
            }   
        } 

        return id;
    }

    /// <summary>Decode device skills (i.e., hardware features provided by the device).</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <param name="skills">Output reference to hardware skills.</param>
    // public static void Dec_DeviceSkills(CommandResponse response, List<KeyValuePair<int, String>> skills)
    // {
    //     skills = new List<KeyValuePair<int, String>>();

    //     // Get skills code to be parsed
    //     UInt32 skillsCode = BitConverter.ToUInt32(response.payload,0);

    //     // Extract hardware skills given the overall skills code
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_GYRO) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_GYRO, "GYR"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_AXL) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_AXL, "AXL"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_MAGN) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_MAGN, "MAG"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_HDR) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_HDR, "HDR"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_TEMP) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_TEMP, "TEMP"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_RH) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_RH, "RH"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_BAR) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_BAR, "BAR"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_LUM_VIS) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_LUM_VIS, "LUM/VIS"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_LUM_IR) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_LUM_IR, "LUM/IR"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_RANGE) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_RANGE, "RANGE"));
    //     if ((skillsCode & (UInt32)HardwareSkills.SKILLS_HW_MIC) > 0)
    //         skills.Add(new KeyValuePair<UInt32, string>((UInt32)HardwareSkills.SKILLS_HW_MIC, "MIC"));
    // }

    /// <summary>Decode memory status information.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Available memory and number of files.</param>
    public static int[] Dec_MemoryStatus(CommandResponse response)
    {
        // Decode current memory status given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_MEM_CONTROL.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                int[] mem_status = new int[2];

                // Get available free memory (i.e., percentage value)
                mem_status[0] = (byte)(response.Payload()[0] & 0xff);   // available_memory

                // Get number of files currently saved in memory
                mem_status[1] = bytesToUInt16(response.Payload(), 1);   // number_of_files  

                return mem_status;
            }
        }

        return null;
    }

    /// <summary>Decode file information.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>FileInfo structure.</param>
    public static FileInfo Dec_FileInfo(CommandResponse response)
    {
        // Decode current file info given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_MEM_FILE_INFO.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                // Decode file timestamp
                byte[] tmp = new byte[8];
                System.arraycopy(response.Payload(), 0, tmp, 0, 5);
                long ts = bytesToLong(tmp,0);
                ts += (Muse_HW.REFERENCE_EPOCH * 1000);

                // Decode sensors full scales

                // Pad 1-bytes response with further 3-bytes before converting to UInt32 and extracting configuration codes
                tmp = new byte[4];
                System.arraycopy(response.Payload(), 5, tmp, 0, 1);
                long code = bytesToUInt32(tmp,0);

                // SensorConfig gyrConfig, axlConfig, magConfig, hdrConfig = null;
                // DecodeMEMSConfiguration(response.payload[5], out gyrConfig, out axlConfig, out magConfig, out hdrConfig);
                SensorConfig[] sensorsConfiguration = DecodeMEMSConfiguration(code); //, gyrConfig, axlConfig, magConfig, hdrConfig);

                // Decode data acquisition mode
                tmp = new byte[4];
                System.arraycopy(response.Payload(), 6, tmp, 0, 3);
                long dm = bytesToUInt32(tmp,0);

                // Update file_info object
                // file_info = new FileInfo(ts, gyrConfig, axlConfig, magConfig, hdrConfig, dm, response.Payload()[9]);
                return new FileInfo(ts, sensorsConfiguration[0], sensorsConfiguration[1], sensorsConfiguration[2], sensorsConfiguration[3], dm, response.Payload()[9]);
            }
        }

        return null;
    }

    public static long Dec_ClockOffset(CommandResponse response)
    {
        long clock_offset = 0;

        // Decode current file info given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_CLK_OFFSET.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
                clock_offset = bytesToLong(response.Payload(),0);
        }

        return clock_offset;
    }

    /// <summary>Decode sensors full scale / sensitivity configuration.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Array of sensor configuration objects (i.e., SensorConfig).</param>
    public static SensorConfig[] Dec_SensorFullScales(CommandResponse response) 
    {
        // Decode current sensors full scales (i.e., gyr / axl / hdr / mag) given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_SENSORS_FS.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                // Pad 3-bytes response.payload array with a further byte before converting to UInt32 and extracting configuration codes
                byte[] tmp = new byte[4];
                System.arraycopy(response.Payload(), 0, tmp, 0, 3);
                long code = bytesToUInt32(tmp,0);

                // Extract MEMS configurations
                return DecodeMEMSConfiguration(code);
            }
        }

        return null;
    }

    public static float[] Dec_CalibrationMatrixValues(byte[] colVal)
    {
        float[] colValues = new float[3];

        byte[] tmp = new byte[4];
        System.arraycopy(colVal, 0, tmp, 0, 4);
        colValues[0] = (float)bytesToInt32(tmp,0);
        System.arraycopy(colVal, 4, tmp, 0, 4);
        colValues[1] = (float)bytesToInt32(tmp,0);
        System.arraycopy(colVal, 8, tmp, 0, 4);
        colValues[2] = (float)bytesToInt32(tmp,0);

        return colValues;
    }

    /// <summary>Decode button log configuration.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>Current mode and frequency configured for button log acquisition.</param>
    public static String[] Dec_ButtonLogConfiguration(CommandResponse response)
    {
        // Decode current sensors full scales (i.e., gyr / axl / hdr / mag) given command response payload
        if ((response.Tx() & 0x7F) == Command.CMD_BUTTON_LOG.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                String[] log_config = new String[2];

                // Pad 3-bytes response.payload array with a further byte before converting to UInt32
                byte[] tmp = new byte[4];
                System.arraycopy(response.Payload(), 0, tmp, 0, 3);

                // Apply bitwise mask to get data acquisition mode combination
                long code = bytesToUInt32(tmp,0);

                // Build acquisition mode string description
                log_config[0] = DataModeToString(code);     // mode

                // Set acquisition frequency string representation
                log_config[1] = DataFrequencyToString(DataFrequency.valueOf(response.Payload()[3])); // frequency

                return log_config;
            }
        }

        return null;
    }

    /// <summary>Decode user configuration packet.</summary>
    /// <param name="response">Input CommandResponse object to be decoded.</param>
    /// <return>User configuration object.</param>
    public static UserConfig Dec_UserConfiguration(CommandResponse response)
    {   
        // Decode user configuration given current command response
        if ((response.Tx() & 0x7F) == Command.CMD_USER_CONFIG.getValue() &&
            response.Ack() == AcknowledgeType.ACK_SUCCESS.getValue())
        {
            if ((response.Tx() & Muse_HW.READ_BIT_MASK) != 0)
            {
                // Get code from which the user configuration must be extracted
                int code = bytesToUInt16(response.Payload(),0);

                // Set internal boolean flags
                boolean standby = false;
                boolean memory = false;
                boolean usb = false;

                if ((code & UserConfigMask.USER_CFG_MASK_USB_STREAM.getValue()) > 0)
                    usb = true;
                if ((code & UserConfigMask.USER_CFG_MASK_CIRCULAR_MEMORY.getValue()) > 0)
                    memory = true;
                if ((code & UserConfigMask.USER_CFG_MASK_STANDBY.getValue()) > 0)
                    standby = true;

                return new UserConfig(standby, memory, usb);
            }
        }

        return null;
    }

    /// <summary>Returns the data packet dimension corresponding to a given data acquisition mode.</summary>
    /// <returns>A integer value representing the data packet dimension.</returns>
    /// <param name="inMode">Data acquisition mode.</param>
    public static int GetPacketDimension(DataMode inMode)
    {
        int packet_dimension = 0;

        // Compute packet dimension given data acquisition mode
        if ((inMode.getValue() & DataMode.DATA_MODE_GYRO.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_GYRO.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_AXL.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_AXL.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_HDR.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_HDR.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_MAGN.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_MAGN.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_ORIENTATION.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_ORIENTATION.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_TIMESTAMP.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_TIMESTAMP.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_TEMP_HUM.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_TEMP_HUM.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_TEMP_PRESS.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_TEMP_PRESS.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_RANGE.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_RANGE.getValue();
        if ((inMode.getValue() & DataMode.DATA_MODE_SOUND.getValue()) > 0)
            packet_dimension += DataSize.DATA_SIZE_SOUND.getValue();

        // Check packet dimension consistency
        // It MUST be a dividend of 120 (2040)
        if (packet_dimension == 6 || packet_dimension == 12 || packet_dimension == 24 ||
                packet_dimension == 30 || packet_dimension == 60)
            return packet_dimension;

        // Return -1 in case of data dimension inconsistency
        return -1;
    }

    /// <summary>Returns the overall number of data packets contained into a 128-bytes data characteristic given a data acquisition mode.</summary>
    /// <returns>A integer value representing the number of data packets.</returns>
    /// <param name="mode">Data acquisition mode.</param>
    public static int GetNumberOfPackets(DataMode mode)
    {
        // Compute packet dimension and check its consistency given data acquisition mode
        int packet_dimension = GetPacketDimension(mode);

        // Compute overall number of packets contained into a data characteristic update
        if (packet_dimension != -1)
            return (120 / packet_dimension);

        // Return -1 in case of data dimension inconsistency
        return -1;
    }

    /// <summary>Parse data characteristic to get an observable collection of muse data objects.</summary>
    /// <returns>An observable collection of muse data objects.</returns>
    /// <param name="mode">Data acquisition mode.</param>
    /// <param name="type">Data acquisition type.</param>
    /// <param name="buffer">Byte array to be parsed.</param>
    /// <param name="overall_packet_dimension">Overal packet dimension based on selected acquisition mode.</param>
    /// <param name="num_of_packets">Number of packets into buffer.</param>
    /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
    /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
    /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
    /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
    public static List<Muse_Data> ParseDataCharacteristic(DataMode mode,
                                AcquisitionType type,
                                byte[] buffer,
                                int overall_packet_dimension,
                                int num_of_packets,
                                float gyr_res, float axl_res, float mag_res, float hdr_res)
    {
        if (buffer != null && buffer.length > 0)
        {
            // Set decoded data buffer to be returned
            List<Muse_Data> data_collection = new ArrayList<Muse_Data>();

            // Manage data decoding based on selected acquisition type
            if (type == AcquisitionType.ACQ_READOUT)
            {
                ///////////////////////////////////////////////////
                // Manage file offload decoding

            }
            else
            {
                ///////////////////////////////////////////////////
                // Decode timestamp from raw data header (5 bytes)
                byte[] currentTime = new byte[8];
                System.arraycopy(buffer, 0, currentTime, 0, 5);
                long currentTime_val = bytesToLong(currentTime,0);
                currentTime_val += (Muse_HW.REFERENCE_EPOCH * 1000);

                // Initially equal to the default length of the message header
                int overall_index_offset = 8;

                ///////////////////////////////////////////////////////////////////////
                // Manage stream decoding based on BUFFERED or DIRECT acquisition type
                if (type == AcquisitionType.ACQ_TX_BUFFERED)
                {
                    for (int i = 0; i < num_of_packets; i++)
                    {
                        // Iterate through the buffer and parse data packets
                        byte[] rawPacket = new byte[overall_packet_dimension];
                        System.arraycopy(buffer, overall_index_offset, rawPacket, 0, overall_packet_dimension);

                        // Decode current data packet
                        Muse_Data current_data = DecodePacket(rawPacket, currentTime_val, mode, gyr_res, axl_res, mag_res, hdr_res);
                        data_collection.add(current_data);

                        // Update overall index offset 
                        overall_index_offset += overall_packet_dimension;
                    }
                }
                else if (type == AcquisitionType.ACQ_TX_DIRECT)
                {
                    // Retrieve the overall payload
                    byte[] rawPacket = new byte[overall_packet_dimension];
                    System.arraycopy(buffer, overall_index_offset, rawPacket, 0, overall_packet_dimension);

                    // Decode current data packet
                    Muse_Data current_data = DecodePacket(rawPacket, currentTime_val, mode, gyr_res, axl_res, mag_res, hdr_res);
                    data_collection.add(current_data);
                }
            }

            return data_collection;
        }

        return null;
    }

    /// <summary>Decode data packet.</summary>
    /// <returns>A muse data object.</returns>
    /// <param name="buffer">Byte array to be decoded.</param>
    /// <param name="timestamp">Reference timestamp related to the last notification.</param>
    /// <param name="mode">Data acquisition mode.</param>
    /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
    /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
    /// <param name="mag_res">Magnetometer sensitivity coefficient.</param>
    /// <param name="hdr_res">High Dynamic Range (HDR) Accelerometer sensitivity coefficient.</param>
    private static Muse_Data DecodePacket(byte[] buffer, long timestamp, DataMode mode, float gyr_res, float axl_res, float mag_res, float hdr_res)
    {
        // Check input buffer consistency
        if (buffer != null && buffer.length > 0)
        {
            // Define muse data container
            int packet_index_offset = 0;
            Muse_Data current_data = new Muse_Data();

            // Set overall timestamp reference
            current_data.SetOverallTimestamp(timestamp);

            // Ther order of IF statements is related to the order with which the data coming within each packet in order to properly update the packet_index_offset
            if ((mode.getValue() & DataMode.DATA_MODE_GYRO.getValue()) > 0)
            {
                // Decode Gyroscope reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_GYRO.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_GYRO.getValue());
                packet_index_offset += DataSize.DATA_SIZE_GYRO.getValue();

                current_data.SetGyroscope(DataTypeGYR(currentPacket, gyr_res));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_AXL.getValue()) > 0)
            {
                // Decode Accelerometer reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_AXL.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_AXL.getValue());
                packet_index_offset += DataSize.DATA_SIZE_AXL.getValue();

                current_data.SetAccelerometer(DataTypeAXL(currentPacket, axl_res));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_MAGN.getValue()) > 0)
            {
                // Decode Magnetometer reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_MAGN.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_MAGN.getValue());
                packet_index_offset += DataSize.DATA_SIZE_MAGN.getValue();

                current_data.SetMagnetometer(DataTypeMAGN(currentPacket, mag_res));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_HDR.getValue()) > 0)
            {
                // Decode Accelerometer HDR reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_HDR.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_HDR.getValue());
                packet_index_offset += DataSize.DATA_SIZE_HDR.getValue();

                current_data.SetHDR(DataTypeHDR(currentPacket, hdr_res));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_ORIENTATION.getValue()) > 0)
            {
                // Decode orientation quaternion
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_ORIENTATION.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_ORIENTATION.getValue());
                packet_index_offset += DataSize.DATA_SIZE_ORIENTATION.getValue();

                current_data.SetQuaternion(DataTypeOrientation(currentPacket));
                current_data.SetEulerAngles(GetAnglesFromQuaternion(current_data.GetQuaternion()));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_TIMESTAMP.getValue()) > 0)
            {
                // Decode timestamp
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_TIMESTAMP.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_TIMESTAMP.getValue());
                packet_index_offset += DataSize.DATA_SIZE_TIMESTAMP.getValue();

                current_data.SetTimestamp(DataTypeTimestamp(currentPacket));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_TEMP_HUM.getValue()) > 0)
            {
                // Decode temperature and humidity reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_TEMP_HUM.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_TEMP_HUM.getValue());
                packet_index_offset += DataSize.DATA_SIZE_TEMP_HUM.getValue();

                current_data.SetTH(DataTypeTempHum(currentPacket));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_TEMP_PRESS.getValue()) > 0)
            {
                // Decode temperature and barometric pressure reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_TEMP_PRESS.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_TEMP_PRESS.getValue());
                packet_index_offset += DataSize.DATA_SIZE_TEMP_PRESS.getValue();

                current_data.SetTP(DataTypeTempPress(currentPacket));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_RANGE.getValue()) > 0)
            {
                // Decode distance / luminosity reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_RANGE.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_RANGE.getValue());
                packet_index_offset += DataSize.DATA_SIZE_RANGE.getValue();

                current_data.SetRange(DataTypeRange(currentPacket));
            }

            if ((mode.getValue() & DataMode.DATA_MODE_SOUND.getValue()) > 0)
            {
                // Decode sound reading
                byte[] currentPacket = new byte[DataSize.DATA_SIZE_SOUND.getValue()];
                System.arraycopy(buffer, packet_index_offset, currentPacket, 0, DataSize.DATA_SIZE_SOUND.getValue());
                // packet_index_offset += DataSize.DATA_SIZE_SOUND;

                current_data.SetSound(DataTypeSound(currentPacket));
            }

            return current_data;
        }
        return null;
    }

    /// <summary>Decode Gyroscope reading.</summary>
    /// <returns>An array of float (i.e., x, y, z axes).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    /// <param name="gyr_res">Gyroscope sensitivity coefficient.</param>
    private static float[] DataTypeGYR(byte[] currentPayload, float gyr_res)
    {
        // Define 3-elements array of float (i.e., x, y, z channels)
        float[] currentData = new float[3];
        // Define temporary internal variable used for data conversion
        byte[] tmp = new byte[2];

        // Iterate across Gyroscope channels
        for (int i = 0; i < 3; i++)
        {
            // Extract channel raw value (i.e., 2-bytes each)
            System.arraycopy(currentPayload, 2*i, tmp, 0, 2);
            // Convert to Int16 and apply sensitivity scaling
            currentData[i] = (float)(bytesToInt16(tmp) * gyr_res);
        }

        // Return decoded Gyroscope reading
        return currentData;
    }

    /// <summary>Decode Accelerometer reading.</summary>
    /// <returns>An array of float (i.e., x, y, z axes).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    /// <param name="axl_res">Accelerometer sensitivity coefficient.</param>
    private static float[] DataTypeAXL(byte[] currentPayload, float axl_res)
    {
        float[] currentData = new float[3];
        byte[] tmp = new byte[2];

        // Accelerometer
        for (int i = 0; i < 3; i++)
        {
            System.arraycopy(currentPayload, 2 * i, tmp, 0, 2);
            currentData[i] = (float)(bytesToInt16(tmp) * axl_res);
        }

        return currentData;
    }

    /// <summary>Decode Magnetometer reading.</summary>
    /// <returns>An array of float (i.e., x, y, z axes).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    /// <param name="mag_res">Accelerometer sensitivity coefficient.</param>
    private static float[] DataTypeMAGN(byte[] currentPayload, float mag_res)
    {
        float[] currentData = new float[3];
        byte[] tmp = new byte[2];

        // Magnetometer
        for (int i = 0; i < 3; i++)
        {
            System.arraycopy(currentPayload, 2 * i, tmp, 0, 2);
            currentData[i] = (float)(bytesToInt16(tmp) * mag_res);
        }

        return currentData;
    }

    /// <summary>Decode High Dynamic Range (HDR) Accelerometer reading.</summary>
    /// <returns>An array of float (i.e., x, y, z axes).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    /// <param name="hdr_res">Accelerometer sensitivity coefficient.</param>
    private static float[] DataTypeHDR(byte[] currentPayload, float hdr_res)
    {
        float[] currentData = new float[3];
        byte[] tmp = new byte[2];

        // Accelerometer HDR
        for (int i = 0; i < 3; i++)
        {
            System.arraycopy(currentPayload, 2 * i, tmp, 0, 2);
            currentData[i] = (float)((bytesToInt16(tmp) / 16) * hdr_res);
        }

        return currentData;
    }

    /// <summary>Decode orientation (unit) quaternion.</summary>
    /// <returns>An array of float (i.e., qw, qi, qj, qk).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    private static float[] DataTypeOrientation(byte[] currentPayload)
    {
        // Define 4-elements array of float (i.e., qw, qi, qj, qk channels)
        float[] currentData = new float[4];

        // // Define temporary internal variable used for data conversion
        // byte[] tmp = new byte[2];

        // // Orientation Quaternion (i.e., UNIT QUATERNION)
        // float[] tempFloat = new float[3];
        // for (int i = 0; i < 3; i++)
        // {
        //     // Extract channel raw value (i.e., 2-bytes each) and convert to Int16
        //     tempFloat[i] = BitConverter.ToInt16(currentPayload, 2*i);

        //     // Keep into account the data resolution
        //     tempFloat[i] /= 32767;
            
        //     // Assign imaginary parts of quaternion
        //     currentData[i + 1] = tempFloat[i];
        // }

        // // Compute real component of quaternion given immaginary parts
        // currentData[0] = (float)Math.sqrt(1 - (currentData[1] * currentData[1] +
        //         currentData[2] * currentData[2] + currentData[3] * currentData[3]));

        // Return decoded Unit Quaternion
        return currentData;
    }

    /// <summary>Compute Euler Angles given a unit quaternion.</summary>
    /// <returns>An array of float (i.e., roll, pitch and yaw).</returns>
    /// <param name="q">Unit quaternion to be converted.</param>
    private static float[] GetAnglesFromQuaternion(float[] q)
    {
        float[] result = new float[3];

        // Compute Euler Angles given a unit quaternion
        result[0] = (float)(Math.atan2(2 * (q[0] * q[1] + q[2] * q[3]), 1 - 2 * (q[1] * q[1] + q[2] * q[2])) * 180 / Math.PI);
        result[1] = (float)(Math.asin(2 * q[0] * q[2] - 2 * q[3] * q[1]) * 180 / Math.PI);
        result[2] = (float)(Math.atan2(2 * (q[0] * q[3] + q[1] * q[2]), 1 - 2 * (q[2] * q[2] + q[3] * q[3])) * 180 / Math.PI);
        
        return result;
    }

    /// <summary>Decode timestamp.</summary>
    /// <returns>A unsigned long integer value representing the timestamp in epoch format.</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    private static long DataTypeTimestamp(byte[] currentPayload)
    {
        // Set current data variable to 0
        long currentData = 0;

        // // Get raw byte array representation to be decoded
        // byte[] tmp = new byte[8];
        // System.arraycopy(currentPayload, 0, tmp, 0, 6);
        
        // // Convert raw data to 64-unsigned integer representation
        // long tempTime = bytesToLong(tmp);
        // tempTime &= 0x0000FFFFFFFFFFFF;

        // // Add reference epoch
        // tempTime += (Muse_HW.REFERENCE_EPOCH * 1000);

        // currentData = tempTime;

        // Return current date/time
        return currentData;
    }

    /// <summary>Decode temperature and humidity reading.</summary>
    /// <returns>An array of float (i.e., temperature, humidity).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    private static float[] DataTypeTempHum(byte[] currentPayload)
    {
        float[] currentData = new float[2];

        // Temperature
        byte[] tmp = new byte[2];
        System.arraycopy(currentPayload, 0, tmp, 0, 2);
        currentData[0] = (float)(bytesToInt16(tmp)) * 0.002670f - 45;

        // Humidity
        System.arraycopy(currentPayload, 2, tmp, 0, 2);
        currentData[1] = (float)(bytesToInt16(tmp)) * 0.001907f - 6;

        return currentData;

    }

    /// <summary>Decode temperature and barometric pressure reading.</summary>
    /// <returns>An array of float (i.e., temperature, pressure).</returns>
    /// <param name="currentPayload">Bytes array to be decoded.</param>
    private static float[] DataTypeTempPress(byte[] currentPayload)
    {
        float[] currentData = new float[2];

        // Temperature
        byte[] tmp = new byte[2];
        System.arraycopy(currentPayload, 3, tmp, 0, 2);
        currentData[0] = (float)(bytesToInt16(tmp)) / 100;

        // Pressure
        tmp = new byte[3];
        System.arraycopy(currentPayload, 0, tmp, 0, 3);
        currentData[1] = (float)(bytesToInt16(tmp)) / 4096;

        return currentData;
    }

    private static Range DataTypeRange(byte[] currentPayload)
    {
        Range currentData = new Range(0,0,0);

        return currentData;
    }

    private static float DataTypeSound(byte[] currentPayload)
    {
        float currentData = 0;

        return currentData;
    }

    // endregion

    /// <summary>Decode sensors configuration in terms of full scale and sensitivity.</summary>
    /// <param name="code">24-bit unsigned integer code.</param>
    /// <return>Array of sensor configuration objects (i.e., SensorConfig)</param>
    public static SensorConfig[] DecodeMEMSConfiguration(long code) {
        SensorConfig[] sensorConfigurations = new SensorConfig[4];

        // Apply bitwise mask to get sensor full scale code (i.e., gyr, axl, hdr, mag LSB-order)
        byte gyrCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_GYRO.getValue());
        byte axlCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_AXL.getValue());
        byte hdrCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_HDR.getValue());
        byte magCode = (byte)(code & (int)MEMS_FullScaleMask.SENSORSFS_MASK_MAGN.getValue());

        // Gyroscope
        sensorConfigurations[0] = Muse_HW.Gyroscope_CFG.get(gyrCode);           // gyrConfig
        // Accelerometer
        sensorConfigurations[1] = Muse_HW.Accelerometer_CFG.get(axlCode);       // axlConfig
        // Magnetometer
        sensorConfigurations[2] = Muse_HW.Magnetometer_CFG.get(magCode);        // magConfig
        // HDR Accelerometer
        sensorConfigurations[3] = Muse_HW.AccelerometerHDR_CFG.get(hdrCode);    // hdrConfig

        return sensorConfigurations;
    }

    /// <summary>Create a string representation of data acquisition mode.</summary>
    /// <returns>A string.</returns>
    /// <param name="code">32-bit unsigned integer code.</param>
    public static String DataModeToString(long code)
    {
        List<String> mdString = new ArrayList<String>();

        // Build acquisition mode string description
        if ((code & DataMode.DATA_MODE_GYRO.getValue()) > 0)
            mdString.add("GYR");
        if ((code & DataMode.DATA_MODE_AXL.getValue()) > 0)
            mdString.add("AXL");
        if ((code & DataMode.DATA_MODE_HDR.getValue()) > 0)
            mdString.add("HDR");
        if ((code & DataMode.DATA_MODE_MAGN.getValue()) > 0)
            mdString.add("MAG");
        if ((code & DataMode.DATA_MODE_ORIENTATION.getValue()) > 0)
            mdString.add("QUAT");
        if ((code & DataMode.DATA_MODE_TIMESTAMP.getValue()) > 0)
            mdString.add("TIME");
        if ((code & DataMode.DATA_MODE_TEMP_HUM.getValue()) > 0)
            mdString.add("TH");
        if ((code & DataMode.DATA_MODE_TEMP_PRESS.getValue()) > 0)
            mdString.add("TP");
        if ((code & DataMode.DATA_MODE_RANGE.getValue()) > 0)
            mdString.add("RANGE");
        if ((code & DataMode.DATA_MODE_SOUND.getValue()) > 0)
            mdString.add("MIC");

        return String.join(", ", mdString);
    }

    /// <summary>Create a string representation of data acquisition frequency.</summary>
    /// <returns>A string.</returns>
    /// <param name="frequency">8-bit unsigned integer code.</param>
    public static String DataFrequencyToString(DataFrequency frequency)
    {
        switch (frequency)
        {
            case DATA_FREQ_25Hz:
                return "25 Hz";
            case DATA_FREQ_50Hz:
                return "50 Hz";
            case DATA_FREQ_100Hz:
                return "100 Hz";
            case DATA_FREQ_200Hz:
                return "200 Hz";
            case DATA_FREQ_400Hz:
                return "400 Hz";
            case DATA_FREQ_800Hz:
                return "800 Hz";
            case DATA_FREQ_1600Hz:
                return "1600 Hz";
            default:
                return "NONE";
        }
    }




    public static byte[] shortToBytes(short x) {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putShort(x);
        return buffer.array();
    }

    public static byte[] intToBytes(int x) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(x);
        return buffer.array();
    }

    public static byte[] longToBytes(long x) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putLong(x);
        return buffer.array();
    }

    public static byte[] floatToBytes(float x) {
        ByteBuffer buffer = ByteBuffer.allocate(Float.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putFloat(x);
        return buffer.array();
    }
    
    public static short bytesToUInt8(byte bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes);
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return (short)(buffer.getShort() & 0xff);
    }

    public static short bytesToInt16(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes);
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return buffer.getShort();
    }

    public static int bytesToUInt16(byte[] bytes, int index) {
        ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes,index,2);
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return buffer.getShort() & 0xffff;
    }

    public static int bytesToInt32(byte[] bytes, int index) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes,index,4);
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return buffer.getInt();
    }

    public static long bytesToUInt32(byte[] bytes, int index) {
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes,index,4);  // 32-bit unsigned integers (4 bytes long)
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return buffer.getInt() & 0xffffffffL; 
    }

    public static long bytesToLong(byte[] bytes, int index) {
        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes);
        buffer.flip();  // Need flip to manage java.nio.BufferUnderflowException
        return buffer.getLong();
    }

    public static float bytesToFloat(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.allocate(Float.BYTES).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(bytes);
        buffer.flip();// Need flip to manage java.nio.BufferUnderflowException
        return buffer.getFloat();
    }

    private static final byte[] HEX_ARRAY = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);
    public static String bytesToHex(byte[] bytes) {
        byte[] hexChars = new byte[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xff;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0f];
        }
        return new String(hexChars, StandardCharsets.UTF_8);
    }

    private static byte[] reverseBytes(byte[] bytes) {
        byte[] buf = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            buf[i] = bytes[bytes.length - 1 - i];
        return buf;
    }
}
