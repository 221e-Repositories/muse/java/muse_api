package _221e.Muse;

import _221e.Muse.Muse_HW.DataFrequency;

/// <summary>File Information structure</summary>
public class FileInfo
{
    private long timestamp; // { get; private set; }
    private SensorConfig gyrConfig; // { get; private set; }
    private SensorConfig axlConfig; // { get; private set; }
    private SensorConfig magConfig; // { get; private set; }
    private SensorConfig hdrConfig; // { get; private set; }
    private long mode; // { get; private set; }
    private String modeString; // { get; private set; }
    private DataFrequency frequency; // { get; private set; }
    private String frequencyString; // { get; private set; }

    /// <summary>Timestamp</summary>
    public long Timestamp() {
        return timestamp;
    }

    /// <summary>Gyroscope configuration</summary>
    public SensorConfig GyrConfig() {
        return gyrConfig;
    } 

    /// <summary>Accelerometer configuration</summary>
    public SensorConfig AxlConfig() {
        return axlConfig;
    }

    /// <summary>Magnetometer configuration</summary>
    public SensorConfig MagConfig() {
        return magConfig;
    }

    /// <summary>HDR Accelerometer configuration</summary>
    public SensorConfig HDRConfig() {
        return hdrConfig;
    }

    /// <summary>Acquisition mode</summary>
    public long Mode() {
        return mode;
    }
    /// <summary>String representation of acquisition mode</summary>
    public String ModeString() {
        return modeString;
    }

    /// <summary>Acquisition frequency</summary>
    public DataFrequency Frequency() {
        return frequency;
    }
    /// <summary>String representation of acquisition frequency</summary>
    public String FrequencyString() {
        return frequencyString;
    }

    /// <summary>FileInfo constructor</summary>
    /// <param name="timestamp">Timestamp</param>
    /// <param name="gyrConfig">Gyroscope configuration</param>
    /// <param name="axlConfig">Accelerometer configuration</param>
    /// <param name="magConfig">Magnetometer configuration</param>
    /// <param name="hdrConfig">HDR Accelerometer configuration</param>
    /// <param name="mode">Acquisition mode</param>
    /// <param name="frequency">Acquisition frequency</param>
    public FileInfo(long timestamp, SensorConfig gyrConfig, SensorConfig axlConfig, SensorConfig magConfig, SensorConfig hdrConfig, long mode, byte frequency)
    {
        this.timestamp = timestamp;

        this.gyrConfig = gyrConfig;
        this.axlConfig = axlConfig;
        this.magConfig = magConfig;
        this.hdrConfig = hdrConfig;

        this.mode = mode;
        this.modeString = Muse_Utils.DataModeToString(mode);

        this.frequency = DataFrequency.valueOf(frequency);
        this.frequencyString = Muse_Utils.DataFrequencyToString(this.frequency);        
    }

    /// <summary>Override of ToString method</summary>
    public String toString()
    {
        return timestamp + ", " + 
            gyrConfig.ToString() + ", " + 
            axlConfig.ToString() + ", " + 
            magConfig.ToString() + ", " + 
            hdrConfig.ToString() + ", " + 
            modeString + ", " + 
            frequencyString;
    }
}
